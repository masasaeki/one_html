class MessagesController < ApplicationController
  def create
    message = Message.new(message_params)
    # message = Message.new(name: nil)
    if message.save
      MessageMailer.with(message: message).welcome_email.deliver_now
      render json: { message: message }, status: :created, location: message
    else
      render json: { errors: message.errors }, status: :unprocessable_entity
    end
  end

  def index
    messages = Message.order(created_at: :desc)
    render json: messages
  end

  def show
    message = Message.find(params[:id])
    render json: message
  end

  private

  def message_params
    # pp request.body.read
    # params = ActionController::Parameters.new(JSON.parse(request.body.read))
    # pp params[:message]
    # pp params[:name]
    # pp params["message"]["name"]
    # params.require(:message).permit(:name, :email, :message)
    params.permit(:name, :email, :message)
    # params.permit(:name, :email, :message)
  end
end
