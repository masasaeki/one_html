class MessageMailer < ApplicationMailer
  
  def welcome_email
    @message = params[:message]
    mail(to: @message.email, bcc: "masaakisaeki@gmail.com", subject: 'お問い合わせありがとうございます。')
  end
end
